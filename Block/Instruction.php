<?php


namespace Kowal\PlatnoscZaPobraniem\Block;

use Kowal\PlatnoscZaPobraniem\Model\InstructionsConfigProvider;

class Instruction extends \Magento\Framework\View\Element\Template
{
    /**
     * @var InstructionsConfigProvider
     */
    protected $configProvider;

    /**
     * Instruction constructor.
     * @param InstructionsConfigProvider $configProvider
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Kowal\PlatnoscZaPobraniem\Model\InstructionsConfigProvider $configProvider,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->configProvider = $configProvider;
    }

    /**
     * @return string
     */
    public function Instruction()
    {
        return $this->configProvider->getConfig();
    }
}