<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Kowal\PlatnoscZaPobraniem\Model\Payment;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;

class InstructionsConfigProvider implements ConfigProviderInterface
{
    /**
     * @var string[]
     */
    protected $methodCode = 'pobranie';

    /**
     * @var PaymentHelper
     */
    protected $paymentHelper;

    /**
     * @var \Magento\Payment\Model\Config
     */
    protected $paymentConfig;

    /**
     * @var \Magento\Payment\Model\Method\AbstractMethod[]
     */
    protected $methods = [];

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * InstructionsConfigProvider constructor.
     * @param PaymentHelper $paymentHelper
     * @param \Magento\Payment\Model\Config $paymentConfig
     * @param Escaper $escaper
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        \Magento\Payment\Model\Config $paymentConfig,
        Escaper $escaper
    )
    {
        $this->paymentHelper = $paymentHelper;
        $this->paymentConfig = $paymentConfig;
        $this->escaper = $escaper;
        $this->methods[$this->methodCode] = $this->paymentHelper->getMethodInstance($this->methodCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [];
        if ($this->methods[$this->methodCode]->isAvailable()) {
            $config['payment']['instructions'][$this->methodCode] = $this->paymentHelper->getMethodInstance($this->methodCode)->getConfigData('instructions', 0);
        }
        return $config;
    }

    /**
     * Get instructions text from config
     *
     * @param string $code
     * @return string
     */
    protected function getInstructions($code)
    {
        return nl2br($this->escaper->escapeHtml($this->methods[$code]->getInstructions()));
    }
}