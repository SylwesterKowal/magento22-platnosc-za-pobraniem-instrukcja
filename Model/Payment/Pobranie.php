<?php


namespace Kowal\PlatnoscZaPobraniem\Model\Payment;

class Pobranie extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "pobranie";
    protected $_isOffline = true;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    )
    {
        return parent::isAvailable($quote);
    }
}
