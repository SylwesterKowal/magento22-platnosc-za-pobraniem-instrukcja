define(
    [
        'Magento_Checkout/js/view/payment/default'
    ],
    function (Component) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Kowal_PlatnoscZaPobraniem/payment/pobranie'
            },
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            getInstructions: function () {
                console.log(window.checkoutConfig.payment.instructions);
                return window.checkoutConfig.payment.instructions[this.item.method];
            },
        });
    }
);
